# RPN Calculator

![screenshot](https://i.imgur.com/DnD5EQR.png)

I was fed up with the calculator that comes pre-installed in my OS so I decided
to make my own. It's based on the HP 50g.

This project was meant to be an exploration in functional programming with Elm
and ElectronJS.