module Main exposing (main)

import Browser exposing (element)
import Browser.Events exposing (onKeyDown)
import Html exposing (Html, button, div, span, text)
import Html.Attributes exposing (class, id)
import Html.Events exposing (onClick)
import Json.Decode
import Keys exposing (Key, keyDecoder)


main =
    element
        { init = init
        , view = view
        , update = update_
        , subscriptions = subscriptions
        }


type alias Model =
    { stack : List Float
    , inbuffer : String
    }



-- Crea el modelo inicial


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model [] "", Cmd.none )



-- Lista todos los mensajes que pueden ser generados por la interfaz o eventos


type
    Msg
    -- Operaciones de pila
    = Clear
    | Drop
    | Swap
    | Put
      -- Operaciones matematicas
    | Add
    | Divide
    | Substract
    | Multiply
    | TextInput String
    | NoOp


update_ : Msg -> Model -> ( Model, Cmd Msg )
update_ msg model =
    ( update msg model, Cmd.none )



-- Actualiza el modelo segun el mensaje que fue recibido


update : Msg -> Model -> Model
update msg model =
    case msg of
        TextInput n ->
            { model | inbuffer = model.inbuffer ++ n }

        Clear ->
            { model | stack = [] }

        Drop ->
            if String.isEmpty model.inbuffer then
                { model | stack = List.drop 1 model.stack }

            else
                { model | inbuffer = String.slice 0 -1 model.inbuffer }

        Swap ->
            case model.stack of
                a :: b :: tail ->
                    { model | stack = b :: a :: tail }

                _ ->
                    model

        Put ->
            case String.toFloat model.inbuffer of
                Just f ->
                    { stack = f :: model.stack
                    , inbuffer = ""
                    }

                Nothing ->
                    case model.stack of
                        x :: tail ->
                            { model | stack = x :: x :: tail }

                        [] ->
                            model

        Add ->
            { model | stack = add model.stack }

        Multiply ->
            { model | stack = multiply model.stack }

        Substract ->
            { model | stack = substract model.stack }

        Divide ->
            { model | stack = divide model.stack }

        NoOp ->
            model


keyToMsg : Key -> Msg
keyToMsg key =
    case key of
        Keys.Backspace ->
            Drop

        Keys.Enter ->
            Put

        Keys.Control ->
            Clear

        Keys.Shift ->
            Swap

        Keys.Num0 ->
            TextInput "0"

        Keys.Num1 ->
            TextInput "1"

        Keys.Num2 ->
            TextInput "2"

        Keys.Num3 ->
            TextInput "3"

        Keys.Num4 ->
            TextInput "4"

        Keys.Num5 ->
            TextInput "5"

        Keys.Num6 ->
            TextInput "6"

        Keys.Num7 ->
            TextInput "7"

        Keys.Num8 ->
            TextInput "8"

        Keys.Num9 ->
            TextInput "9"

        Keys.Asterisk ->
            Multiply

        Keys.ForwardSlash ->
            Divide

        Keys.Minus ->
            Substract

        Keys.Plus ->
            Add

        Keys.Dot ->
            TextInput "."

        Keys.Other ->
            NoOp


subscriptions : Model -> Sub Msg
subscriptions _ =
    onKeyDown (Json.Decode.map keyToMsg keyDecoder)



-- Construye la vista en funcion del modelo


view : Model -> Html Msg
view model =
    div [ id "container" ]
        [ div [ id "header" ] [ solarPanel, logo ]
        , screen model
        , button [ id "btn7", onClick (TextInput "7") ] [ text "7" ]
        , button [ id "btn8", onClick (TextInput "8") ] [ text "8" ]
        , button [ id "btn9", onClick (TextInput "9") ] [ text "9" ]
        , button [ class "operation-button", id "btnDiv", onClick Divide ] [ text "÷" ]
        , button [ class "operation-button", id "btnMul", onClick Multiply ] [ text "×" ]
        , button [ id "btn4", onClick (TextInput "4") ] [ text "4" ]
        , button [ id "btn5", onClick (TextInput "5") ] [ text "5" ]
        , button [ id "btn6", onClick (TextInput "6") ] [ text "6" ]
        , button [ class "operation-button", id "btnSub", onClick Substract ] [ text "−" ]
        , button [ class "operation-button", id "btnAdd", onClick Add ] [ text "+" ]
        , button [ id "btn1", onClick (TextInput "1") ] [ text "1" ]
        , button [ id "btn2", onClick (TextInput "2") ] [ text "2" ]
        , button [ id "btn3", onClick (TextInput "3") ] [ text "3" ]
        , button [ class "stack-button", id "btnSwp", onClick Swap ] [ text "SWP" ]
        , button [ class "stack-button", id "btnClr", onClick Clear ] [ text "CLR" ]
        , button [ id "btn0", onClick (TextInput "0") ] [ text "0" ]
        , button [ id "btnDot", onClick (TextInput ".") ] [ text "." ]
        , button [ class "stack-button", id "btnRet", onClick Put ] [ text "RET" ]
        , button [ class "stack-button", id "btnDrp", onClick Drop ] [ text "DRP" ]
        ]


solarPanel : Html msg
solarPanel =
    div [ id "solar_panel" ]
        [ span [ id "solar_left_cell" ] []
        , span [ id "solar_middle_cell" ] []
        , span [ id "solar_right_cell" ] []
        ]


logo : Html msg
logo =
    span [ id "logo" ] [ text "RMB 50g" ]


screen : Model -> Html msg
screen model =
    if String.isEmpty model.inbuffer then
        div [ id "screen" ]
            (List.take 8 model.stack
                |> List.map String.fromFloat
                |> pad 8 ""
                |> rows
            )

    else
        div [ id "screen" ]
            ((List.take 7 model.stack
                |> List.map String.fromFloat
                |> pad 7 ""
                |> rows
             )
                ++ [ span [] [ text model.inbuffer ] ]
            )


pad : Int -> a -> List a -> List a
pad upTo padValue list =
    if List.length list < upTo then
        list ++ List.repeat (upTo - List.length list) padValue

    else
        list


rows : List String -> List (Html msg)
rows values =
    List.indexedMap Tuple.pair values
        |> List.map (\( i, v ) -> ( i + 1, v ))
        -- Incrementar el valor del indice
        |> List.map row
        |> List.reverse


row : ( Int, String ) -> Html msg
row ( index, val ) =
    div [ class "row" ]
        [ span [ class "row-index" ] [ text (String.fromInt index ++ ":") ]
        , span [ class "row-value" ] [ text val ]
        ]


add : List Float -> List Float
add floats =
    case floats of
        a :: b :: tail ->
            (a + b) :: tail

        _ ->
            floats


multiply : List Float -> List Float
multiply floats =
    case floats of
        a :: b :: tail ->
            (a * b) :: tail

        _ ->
            floats


divide : List Float -> List Float
divide floats =
    case floats of
        a :: b :: tail ->
            (b / a) :: tail

        _ ->
            floats


substract : List Float -> List Float
substract floats =
    case floats of
        a :: b :: tail ->
            (b - a) :: tail

        _ ->
            floats
