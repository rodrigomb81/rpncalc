module Keys exposing
    ( main
    , Key (..)
    , keyDecoder
    )

import Browser exposing (element)
import Browser.Events exposing (onKeyDown)
import Html exposing (Html, div, span, text)
import Json.Decode exposing (..)


main =
    element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }

type Key
    = Backspace
    | Enter
    | Control
    | Shift
    | Num0
    | Num1
    | Num2
    | Num3
    | Num4
    | Num5
    | Num6
    | Num7
    | Num8
    | Num9
    | Asterisk
    | ForwardSlash
    | Minus
    | Plus
    | Dot
    | Other

keyPressDecoder =
    map KeyPressed keyDecoder


keyDecoder : Decoder Key
keyDecoder =
    map keyFromString (field "key" string)

keyFromString : String -> Key
keyFromString keyString =
    case keyString of
        "Enter" ->
            Enter

        "Backspace" ->
            Backspace

        "Shift" ->
            Shift

        "Control" ->
            Control

        "/" ->
            ForwardSlash

        "*" ->
            Asterisk

        "-" ->
            Minus

        "+" ->
            Plus

        "." ->
            Dot
        
        "0" ->
            Num0

        "1" ->
            Num1

        "2" ->
            Num2

        "3" ->
            Num3

        "4" ->
            Num4

        "5" ->
            Num5

        "6" ->
            Num6

        "7" ->
            Num7

        "8" ->
            Num8

        "9" ->
            Num9

        _ ->
            Other


type alias Model =
    ( String, Int )


type Msg
    = KeyPressed Key


init : () -> ( Model, Cmd Msg )
init _ =
    ( ( "", 0 ), Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ( _, count ) =
    case msg of
        KeyPressed key ->
            ( ( Debug.toString key, count + 1 ), Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    onKeyDown keyPressDecoder


view : Model -> Html msg
view ( str, count ) =
    div []
        [ span [] [ text (String.fromInt count) ]
        , Html.br [] []
        , span [] [ text str ]
        ]
